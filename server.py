from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit
import random

app = Flask(__name__)
app.config['SECRET_KEY'] = 'gambit'
socketio = SocketIO(app)

quotes = ["Say hello to my lil' friend..", "I feel the need - the need for speed", "Hello Clarice..", "Yo, Adrienne", "I love the smell of napalm in the morning. It smells like victory", "Back off, man. I'm a scientist", "Tell me something, my friend. You ever dance with the devil in the pale moonlight?", "I do wish we could chat longer, but I'm having an old friend for dinner"]

@app.route('/')
def index():
	return render_template('index.html');

@socketio.on('connect', namespace='/websocket')
def socket_connect():
	emit('app response', {'data': 'You connected..'})

@socketio.on('from client', namespace='/websocket')
def respond_back(message):
	print 'Got a message: ' + str(message)
	emit('app response', {'data': random.choice(quotes)})

if __name__ == '__main__':
	socketio.run(app)