$(document).ready(function(){
	var socket = io.connect('http://' + document.domain + ':' + location.port + '/websocket');
	
	socket.on('app response', function(msg){
		$('#messages').append("<p class='bg-success'>Server: " + msg.data + "</p>")
	});



	$('form').on('submit', function(e){
		e.preventDefault();
		var myMSG = $('#messageData').val();
		$('#messageData').val("")
		socket.emit('from client', {data: myMSG});
		$('#messages').append("<p'>Me: " + myMSG + "</p>");
	});
});

