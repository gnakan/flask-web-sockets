#Overview
flask-web-sockets is a simple Flask and webSockets app to serve as a quick tutorial on using websockets in Flask. Allows you to interact the server, which will return back a random movie quote.

Run it with ```python server.py```

#Libraries
[https://flask-socketio.readthedocs.org/en/latest/] flask-SocketIO

#Front End
Basic Bootstrap, javascript and jQuery